package com.rohit.contactsapp.Others;

public class Common {

    public static String reqBody (String user_session_value, String user_id_value, String module_name){
        return "{\"session\":\""+user_session_value+"\"," +
                "\"module_name\":\""+module_name+"\"," +
                "\"query\":\"assigned_user_id=\""+user_id_value+"\"," +
                "\"order_by\":\"status\",\"offset\":\"0\"," +
                "\"max_results\":\"50\",\"deleted\":\"0\"," +
                "\"Favorites\":false}";
    }

}
