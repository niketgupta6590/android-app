package com.rohit.contactsapp.Network;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceGenerator {

    private static final String BASE_URL_List = "http://uat.ideadunes.com/projects/devs/testapi_giproperties/";
    private static final String BASE_URL_LOGIN = "http://hosting.ideadunes.com/hosting/giproperties.ae/service/v4_1/";
    private static ServiceGenerator instance;

    public static ServiceGenerator getInstance(){
        if(instance == null){
            instance = new ServiceGenerator();
        }
        return instance;
    }

    //This is for  BASE_URL_LOGIN
    private static Retrofit retrofit(){
        return new Retrofit.Builder()
                .baseUrl(BASE_URL_LOGIN)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    //This is for fetch List
    private static Retrofit mRetrofit(){
        return new Retrofit.Builder()
                .baseUrl(BASE_URL_List)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static ApiInterface getApi() {
        return  retrofit().create(ApiInterface.class);
    }

    public static ApiInterface getApiforlist() {
        return  mRetrofit().create(ApiInterface.class);
    }
}


