package com.rohit.contactsapp.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Switch;
import android.widget.TextView;

import com.rohit.contactsapp.Activities.Navigation_Drawer;
import com.rohit.contactsapp.R;
import com.rohit.contactsapp.models.Modules;

import java.io.Serializable;
import java.util.ArrayList;


public class Coustom_Adapter2 extends BaseAdapter implements Serializable {

    Context context;
    ArrayList<Modules> arrayList;
    Navigation_Drawer navigation_drawer;
    int resources;
    String modues;
    public ArrayList<String> sendCoustom_module = new ArrayList<>();

    public Coustom_Adapter2(Context context, ArrayList<Modules> arrayList, int resources) {
        this.context = context;
        this.arrayList = arrayList;
        this.resources = resources;

    }

    public ArrayList<String> getStringList() {
        ArrayList<String> strings = null;
        return strings;
    }

    public String getCars() {
        return modues;
    }

    public void setCars(String modues) {
        this.modues = modues;
    }


    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView ==  null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.listof, parent, false);
            final TextView name, key;
            final Switch aswitch;
            aswitch = (Switch)convertView.findViewById(R.id.switch1);
            name = (TextView) convertView.findViewById(R.id.placeholder);
            name.setText(arrayList.get(position).getModule_label());
            aswitch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (aswitch.isChecked()) {
                        arrayList.get(position).setFavorite_enabled(true);
                        modues = (String) name.getText();
                        sendCoustom_module.add(modues);
                        Log.d("tage",sendCoustom_module.toString());

                    }
                    else {
                        arrayList.get(position).setFavorite_enabled(false);
                        modues = (String) name.getText();
                        sendCoustom_module.remove(modues.toString());
                    }

                }
            });
        }
        return convertView;
    }

    private ArrayList<String> sendData() {
        return sendCoustom_module;
    }

    public void setCheckedItem(int item) {


    }






}