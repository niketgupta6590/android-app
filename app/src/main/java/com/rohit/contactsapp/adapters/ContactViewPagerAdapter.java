package com.rohit.contactsapp.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.rohit.contactsapp.fragments.FragmentContact;
import com.rohit.contactsapp.fragments.FragmentCrmContacts;

public class ContactViewPagerAdapter extends FragmentPagerAdapter {
    //Auther Dhanashri shisode
    private Context myContext;
    int totalTabs;

    public ContactViewPagerAdapter(Context context, FragmentManager fm, int totalTabs) {
        super(fm);
        myContext = context;
        this.totalTabs = totalTabs;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                FragmentContact contactFragment = new FragmentContact();
                return contactFragment;
            default:
                FragmentCrmContacts fragmentCrmContacts = new FragmentCrmContacts();
                return fragmentCrmContacts;
        }
    }

    @Override
    public int getCount() {
        return totalTabs;
    }
}

