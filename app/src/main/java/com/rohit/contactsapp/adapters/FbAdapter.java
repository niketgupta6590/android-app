package com.rohit.contactsapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.rohit.contactsapp.models.Facebookfriends;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.rohit.contactsapp.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

//Created by Parvesh Sharma on date 22/10/2020.

public class FbAdapter extends RecyclerView.Adapter<FbAdapter.MyViewHolder> {
    private ArrayList<Facebookfriends> Facebookfriends;

    public FbAdapter(ArrayList<Facebookfriends> facebookfriends) {
        Facebookfriends = facebookfriends;
    }

    private Object flase;
    Context context;

    FbAdapter(Context context){
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.rv_item,parent,false);
        MyViewHolder viewholder = new MyViewHolder(view);
        return viewholder;
    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Picasso.with(context).load("https://graph.facebook.com/"+Facebookfriends.get(position).getId()+"/picture?type=large").into(holder.imageView);
       holder.textView.setText(Facebookfriends.get(position).getName());

    }

    @Override
    public int getItemCount() {
        return Facebookfriends.size();
    }
    public void clear(){
        Facebookfriends.clear();
        notifyDataSetChanged();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{
        ImageView imageView;
        TextView textView;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.iv_friendpic);
            textView = itemView.findViewById(R.id.tv_friend_name);
        }



    }




}
