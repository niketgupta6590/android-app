package com.rohit.contactsapp.adapters;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.rohit.contactsapp.R;
import com.rohit.contactsapp.Activities.DashboardActivity;
import com.rohit.contactsapp.models.Modelsforfeeds;

import java.util.ArrayList;

public class FeedAdapter extends RecyclerView.Adapter<FeedAdapter.MyViewHolder>{
    private ArrayList<Modelsforfeeds> dataSet;



    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView name,post_date,post;
        public MyViewHolder(View itemView) {
            super(itemView);
            this.name = (TextView) itemView.findViewById(R.id.post_name);
            this.post = (TextView) itemView.findViewById(R.id.post);
            this.post_date = (TextView) itemView.findViewById(R.id.publish_date);
        }
    }
    public FeedAdapter(ArrayList<Modelsforfeeds> data, DashboardActivity mainActivity) {
        this.dataSet = data;
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.feeds_card, viewGroup, false);
        view.setOnClickListener(DashboardActivity.myOnClickListener);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {

        TextView name = myViewHolder.name;
        TextView post_date = myViewHolder.post_date;
        TextView post = myViewHolder.post;

        name.setText(dataSet.get(i).getName());

        post.setText(dataSet.get(i).getFeed());

        post_date.setText(dataSet.get(i).getPost_date());

    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

}