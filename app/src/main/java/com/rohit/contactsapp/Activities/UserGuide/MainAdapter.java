package com.rohit.contactsapp.Activities.UserGuide;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.rohit.contactsapp.R;

public class MainAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater inflater;
    private String[] numberwords;
    private int[] numberImage;


    public MainAdapter(Context c,String[] numberwords,int[] numberImage){
        context=c;
        this.numberwords=numberwords;
        this.numberImage=numberImage;
    }

    @Override
    public int getCount() {
        return numberwords.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertview, ViewGroup parent) {
        if(inflater==null){
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if(convertview == null)
        {
            convertview=inflater.inflate(R.layout.row_item,null) ;
        }
        ImageView imageView = convertview.findViewById(R.id.image_view);
        TextView textView = convertview.findViewById(R.id.text_view);


        imageView.setImageResource(numberImage [position]);
        textView.setText(numberwords[position]);

        return convertview;
    }
}

