package com.rohit.contactsapp.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rohit.contactsapp.Others.Common;
import com.rohit.contactsapp.Network.ServiceGenerator;
import com.rohit.contactsapp.R;
import com.rohit.contactsapp.adapters.EmailListAdapter;
import com.rohit.contactsapp.models.EmailsResponses.EmailListResponse;

import java.util.HashMap;
import java.util.Map;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FetchEmails extends AppCompatActivity {

   final static private String TAG = "FetchEmails";

    RecyclerView rvEmails;
    private EmailListResponse emailLists;
    EmailListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fetch_emails);

        final SharedPreferences mSharedPreference= PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        String user_id_value=(mSharedPreference.getString("user_id", null));
        String user_session_value=(mSharedPreference.getString("id", null));

        rvEmails = findViewById(R.id.recylerview_fetch_emails);
        rvEmails.setLayoutManager(new LinearLayoutManager(this));


//        String emailsAuthString =
//                "{\"session\":\""+user_session_value+
//                        "\",\"module_name\":\"Emails\",\"query\":\"assigned_user_id=\""+user_id_value+
//                        "\",\"order_by\":\"status\",\"offset\":\"0\"," +
//                        "\"max_results\":\"50\",\"deleted\":\"0\"," +
//                        "\"Favorites\":false}";
//        try {
//            JSONObject jObject = new JSONObject(emailsAuthString);
//            Log.i(TAG, "email:\n"+jObject);
//        }
//        catch (Exception err){
//            Log.i(TAG, "email: "+"error");
//        }

        Map<String, Object> body = new HashMap<>();
        body.put("input_type", "JSON");
        body.put("response_type", "JSON");
        body.put("method", "get_entry_list");
        body.put("rest_data", Common.reqBody(user_session_value,user_id_value,"Emails"));

        ServiceGenerator.getApiforlist().fetchEmails2().enqueue(new Callback<EmailListResponse>() {

            @Override
            public void onResponse(Call<EmailListResponse> call, Response<EmailListResponse> response) {

                if (response.code() == 200) {
                    EmailListResponse res = response.body();
                    Log.i(TAG,"----------->>"+res);
                    rvEmails.setAdapter(new EmailListAdapter(FetchEmails.this,emailLists.getEntryList()));

               //     startActivity(new Intent(FetchEmails.this, home.class));

                }  {
                    Log.i(TAG, "onerrror: " + response.code() + "\n"+ response.message());

                }
            }
            @Override
            public void onFailure(Call<EmailListResponse> call, Throwable t) {
                Log.e(TAG, "onerrrorfailed: " + t.getMessage());


            }
        });
    }

}
