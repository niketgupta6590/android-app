package com.rohit.contactsapp.Activities.UserGuide;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.rohit.contactsapp.R;

public class SendSuggestion extends AppCompatActivity {

    TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.send_suggestion);
        text= findViewById(R.id.text);
    }
}