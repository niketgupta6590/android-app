package com.rohit.contactsapp.Activities;

import androidx.appcompat.app.AppCompatActivity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import com.rohit.contactsapp.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class ContactsDetailsActivity extends AppCompatActivity {

    //Auther Dhanashri shisode
    TextView c_id, name, number;
    CircleImageView imageView;
    ImageButton sendMassage, callButton, camera;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts_details);

        name = findViewById(R.id.contact_name);
        number = findViewById(R.id.contact_no);
        imageView = findViewById(R.id.contact_person_image);

        sendMassage = findViewById(R.id.chat_image);
        callButton = findViewById(R.id.call_image);
      //  camera = findViewById(R.id.camera_image);

      //  imageView = findViewById(R.id.contact_person_image);

        Intent intent = getIntent();
        name.setText(intent.getStringExtra("contactPerson"));
        number.setText(intent.getStringExtra("contactNo"));
        imageView.setImageBitmap((Bitmap) getIntent().getParcelableExtra("contactImage"));
        //imageView.getIntent().getParcelableExtra("bmp");

        // c_id.setText(intent.getStringExtra("contactId"));


        callButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    String str=number.getText().toString();
                    System.out.println(str);
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"+str));
                    startActivity(intent);

                    Toast.makeText(getApplicationContext(),"Calling",Toast.LENGTH_LONG).show();
                }
                catch (ActivityNotFoundException e){

                    Toast.makeText(getApplicationContext(),"App failed",Toast.LENGTH_LONG).show();
                }
            }
        });

        sendMassage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }
}

