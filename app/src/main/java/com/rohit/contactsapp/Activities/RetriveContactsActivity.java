package com.rohit.contactsapp.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.app.ProgressDialog;
import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;
import com.rohit.contactsapp.R;
import com.rohit.contactsapp.adapters.ContactViewPagerAdapter;

public class RetriveContactsActivity extends AppCompatActivity {
     //Auther Dhanashri shisode
    private TabLayout tabLayout;
    private ViewPager viewPager;

        private void configureView () {
            tabLayout = findViewById(R.id.tab_layout);
            viewPager = (ViewPager) findViewById(R.id.view_pager);

            tabLayout.addTab(tabLayout.newTab().setText("Contacts"));
            tabLayout.addTab(tabLayout.newTab().setText("CRM Contacts"));
            tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

            final ContactViewPagerAdapter pagerAdapter = new ContactViewPagerAdapter(this, getSupportFragmentManager(), tabLayout.getTabCount());
            viewPager.setAdapter(pagerAdapter);

            viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

            tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    viewPager.setCurrentItem(tab.getPosition());
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {

                }
            });
        }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_retrive_contacts);
        configureView();
        }
    }

