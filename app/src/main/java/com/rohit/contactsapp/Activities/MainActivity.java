package com.rohit.contactsapp.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.rohit.contactsapp.R;
import com.rohit.contactsapp.adapters.FbAdapter;
import com.rohit.contactsapp.models.Facebookfriends;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    ImageView imageViewMyPic;
    TextView textViewMyName;
    RecyclerView recyclerView;
    FbAdapter myAdapter;
    RecyclerView.LayoutManager layoutManager;
    AccessTokenTracker accessTokenTracker;
    Button btn_login, btn_signup;
    CallbackManager callbackManager;
    private LoginButton login;
    private int name;

    private Object textViewLogIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fb__friends);

        login = findViewById(R.id.login);
        imageViewMyPic = findViewById(R.id.rv_profile_pic);
        textViewMyName = findViewById(R.id.tv_my_name);
        recyclerView = findViewById(R.id.rv_friend_list);
        textViewMyName.setVisibility(View.INVISIBLE);
        imageViewMyPic.setVisibility(View.INVISIBLE);
        CallbackManager callbackManager = CallbackManager.Factory.create();
//      login.setPermissions("user_friends");
        login.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                textViewMyName.setVisibility(View.VISIBLE);
                imageViewMyPic.setVisibility(View.VISIBLE);

            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });


        btn_login = findViewById(R.id.login);
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(in);
            }
        });
        btn_signup = findViewById(R.id.signup);
        btn_signup.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {
                Intent in = new Intent(MainActivity.this, signup.class);
                startActivity(in);

            }
        });

        CallbackManager CallbackManager = com.facebook.CallbackManager.Factory.create();
        login.registerCallback(CallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                String imageURL = "https://graph.facebook.com/" + loginResult.getAccessToken().getUserId() + "/picture?return_ssl_resources=1";

            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        accessTokenTracker.stopTracking();
    }

    /**
     * Dispatch incoming result to the correct fragment.
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

        GraphRequest graphRequestFriends = GraphRequest.newMyFriendsRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONArrayCallback() {
            @Override
            public void onCompleted(JSONArray objects, GraphResponse response) {
                Log.d("Demo", objects.toString());
                ArrayList<Facebookfriends> Facebookfriends = new ArrayList<>();
                for (int i = 0; i < objects.length(); i++) {
                    try {
                        JSONObject object = objects.getJSONObject(i);
                        Facebookfriends.add(new Facebookfriends(object.getString("id"), object.getString("name")));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
                layoutManager = new LinearLayoutManager(MainActivity.this);
                recyclerView.setLayoutManager(layoutManager);
                myAdapter = new FbAdapter(Facebookfriends);
                recyclerView.setAdapter(myAdapter);


            }
        });
        graphRequestFriends.executeAsync();
        GraphRequest graphRequest = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                try {
                    String name = object.getString("name");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    String id = object.getString("id");
                    textViewMyName.setText(name);
                    Picasso.with(getApplicationContext()).load("https://graph.facebook.com/" + id + "/picture?type=large").into(imageViewMyPic);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        graphRequest.executeAsync();
        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
                if (currentAccessToken == null) ;
                {
                    myAdapter.clear();
                    imageViewMyPic.setVisibility(View.INVISIBLE);
                    textViewMyName.setVisibility(View.INVISIBLE);
                    LoginManager.getInstance().logOut();
                }
            }


        };


    }
}