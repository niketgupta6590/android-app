package com.rohit.contactsapp.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.rohit.contactsapp.R;
import com.rohit.contactsapp.adapters.SliderAdapter;

public class slider_activity extends AppCompatActivity {

    private LinearLayout dotlayout;
    private ViewPager pageslider;

    private TextView[] mDots;
    int mCurrentPage;

    private SliderAdapter sliderAdapter;

    private Button nextbt;
    private  Button prevbt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.slider_layout);

        dotlayout = findViewById(R.id.threedot);
        pageslider = findViewById(R.id.pager);

        sliderAdapter = new SliderAdapter(this);
        pageslider.setAdapter(sliderAdapter);
        addDotsIndicator(0);
        pageslider.addOnPageChangeListener(viewListner);
        nextbt = findViewById(R.id.nextbt);
        prevbt = findViewById(R.id.prevbt);

        nextbt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pageslider.setCurrentItem(mCurrentPage + 1);
            }
        });

        prevbt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pageslider.setCurrentItem(mCurrentPage - 1);
            }
        });





    }

    public void addDotsIndicator(int position){

        mDots  = new TextView[sliderAdapter.slide_images.length];
        dotlayout.removeAllViews();
        for(int i = 0; i < mDots.length; i++){

            mDots[i] = new TextView(this);
            mDots[i].setText(Html.fromHtml("&#8226;"));
            mDots[i].setTextSize(35);
            mDots[i].setTextColor(getResources().getColor(R.color.colorTransparentWhite));

            dotlayout.addView(mDots[i]);
        }

        if(mDots.length > 0 ){
            mDots[position].setTextColor(getResources().getColor(R.color.colorWhite));
        }

    }
    ViewPager.OnPageChangeListener viewListner = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int i) {
            addDotsIndicator(i);

            mCurrentPage = i;
            if (i == 0) {
                nextbt.setEnabled(true);
                prevbt.setEnabled(false);
                prevbt.setVisibility(View.INVISIBLE);

                nextbt.setText("Next");
                prevbt.setText("");
            } else if (i == mDots.length - 1) {
                nextbt.setEnabled(true);
                prevbt.setEnabled(true);
                prevbt.setVisibility(View.VISIBLE);

                nextbt.setText("Agree");
                prevbt.setText("Back");
            } else {
                nextbt.setEnabled(true);
                prevbt.setEnabled(true);
                prevbt.setVisibility(View.VISIBLE);

                nextbt.setText("Next");
                prevbt.setText("Back");
            }

            if (mCurrentPage == 3) {
                nextbt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(slider_activity.this, DashboardActivity.class);
                        startActivity(intent);
                    }

                });
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };
}
