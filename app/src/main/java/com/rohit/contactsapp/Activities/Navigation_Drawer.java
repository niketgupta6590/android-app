package com.rohit.contactsapp.Activities;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.rohit.contactsapp.Activities.UserGuide.HelpAndFeedback;
import com.rohit.contactsapp.R;
import com.rohit.contactsapp.adapters.Coustom_Adapter2;
import java.util.ArrayList;

public class Navigation_Drawer extends AppCompatActivity implements View.OnClickListener{
    DrawerLayout drawerLayout;
    androidx.appcompat.widget.Toolbar toolbar;
    ActionBarDrawerToggle actionBarDrawerToggle;
    ArrayList<String> str = new ArrayList<String>();
    Coustom_Adapter2 adapter;

    TextView coustomModules;
    ListView listView;
    Button refresh;
    TextView username_tv, about, setting, share,desktop,help_and_feedback_tv,social_media;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer);

        desktop = findViewById(R.id.desktop);
        username_tv = findViewById(R.id.username_tv);
        about = findViewById(R.id.about_tv);
        setting = findViewById(R.id.setting);
        share = findViewById(R.id.share_tv);
        listView = (ListView) findViewById(R.id.listview);
        refresh = (Button) findViewById(R.id.refresh);
        help_and_feedback_tv = findViewById(R.id.help_and_feedback_tv);
        social_media = findViewById(R.id.socialmedia);


        help_and_feedback_tv.setOnClickListener(this);
        setting.setOnClickListener(this);
        about.setOnClickListener(this);
        share.setOnClickListener(this);
        desktop.setOnClickListener(this);
        social_media.setOnClickListener(this);



        //////added by oshin on date 21-10-2020
        final SharedPreferences mSharedPreference= PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        String user_name_value=(mSharedPreference.getString("user_name", null));
        username_tv.setText(user_name_value);

        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listView != null)
                addData();
                else
                    Toast.makeText(Navigation_Drawer.this, "Select modules", Toast.LENGTH_SHORT).show();
            }
        });

        str.add("Home");
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(
                this, android.R.layout.simple_list_item_1, str);
        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                askPermission();

                switch (position)
               {
                   case 0:
                       drawerLayout.closeDrawers();
                       Intent intentHome = new Intent(Navigation_Drawer.this,home.class);
                       startActivity(intentHome);
                       drawerLayout.closeDrawers();

                       break;
                   case 1:
                       drawerLayout.closeDrawers();
                       Intent intentLeads = new Intent(Navigation_Drawer.this,RetriveContactsActivity.class);
                       startActivity(intentLeads);
                       drawerLayout.closeDrawers();

                       break;
                   case 2:
                       drawerLayout.closeDrawers();

                       Intent intentCalls = new Intent(Navigation_Drawer.this,ContactsDetailsActivity.class);
                       startActivity(intentCalls);
                       drawerLayout.closeDrawers();

                       break;

                   case 3:
                       Intent intentContact = new Intent(Navigation_Drawer.this, CallLogActivity.class);
                       startActivity(intentContact);
                       drawerLayout.closeDrawers();

                       break;

                   case 4:
                       Intent intentEmails = new Intent(Navigation_Drawer.this, FetchEmails.class);
                       startActivity(intentEmails);
                       drawerLayout.closeDrawers();
                       break;
                   case 5:
                       Intent intentNew  = new Intent(Navigation_Drawer.this, CreateNewContact.class);
                       startActivity(intentNew);
                       drawerLayout.closeDrawers();
                       break;



               }
            }
        });



        coustomModules = findViewById(R.id.CoustomModule);
        coustomModules.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Navigation_Drawer.this, CoustomModulename.class);
                startActivityForResult(intent,2);
            }
        });
        setToolbar();
    }

    private void askPermission() {


            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CALL_LOG)!= PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.READ_CONTACTS},1);
                ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.READ_CALL_LOG},1);
            }



    }


   /* @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if(requestCode==2)
        {
            str = (ArrayList<String>) getIntent().getSerializableExtra("key");

            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                    this,
                    android.R.layout.simple_list_item_1,
                    str);
            listView.setAdapter(arrayAdapter);
            adapter.notifyDataSetChanged();

        }
    }*/





    public void addData(){

        str = (ArrayList<String>) getIntent().getSerializableExtra("key");

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_list_item_1, str);
        listView.setAdapter(arrayAdapter);


    }


    private void openNewActivity() {
        Intent intent = new Intent(this, CoustomModulename.class);
        startActivity(intent);
    }

    public void setToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.app_name, R.string.app_name);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.about_tv:
                startActivity(new Intent(Navigation_Drawer.this,ShareActivity.class));

            case R.id.share_tv:
                startActivity(new Intent(Navigation_Drawer.this,ShareActivity.class));

            case R.id.desktop:
                startActivity(new Intent(Navigation_Drawer.this,DashboardActivity.class));

            case R.id.setting:
                startActivity(new Intent(Navigation_Drawer.this,SettingActivity.class));

            case R.id.help_and_feedback_tv:
                startActivity(new Intent(Navigation_Drawer.this, HelpAndFeedback.class));

            case R.id.socialmedia:
                startActivity(new Intent(Navigation_Drawer.this, SocialApp.class));

        }

    }

    // public void update(){


    /* public void menu_item() {
     Retrofit retrofit = new Retrofit.Builder()
     .baseUrl("https://uat.ideadunes.com/projects/devs/testapi_giproperties/")
     .addConverterFactory(GsonConverterFactory.create())
     .build();
     ApiInterface apiInterface = retrofit.create(ApiInterface.class);
     Call<Models> call = apiInterface.getModels();
     call.enqueue(new Callback<Models>() {
    @Override public void onResponse(Call<Models> call, Response<Models> response) {
    if (response.isSuccessful()) {
    modules = response.body().getModules();
    adapter = new CoustomAdapter(getApplicationContext(), modules, R.layout.listof);
    listView.setAdapter(adapter);
    }
    }

    @Override public void onFailure(Call<Models> call, Throwable t) {
    }
    });
     }  **/
}







