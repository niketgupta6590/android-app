package com.rohit.contactsapp.models.EmailsResponses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NameValueList {
    @Expose
    @SerializedName("opt_in")
    private OptIn optIn;
    @Expose
    @SerializedName("emails_email_templates_name")
    private EmailsEmailTemplatesName emailsEmailTemplatesName;
    @Expose
    @SerializedName("category_id")
    private CategoryId categoryId;
    @Expose
    @SerializedName("is_only_plain_text")
    private IsOnlyPlainText isOnlyPlainText;
    @Expose
    @SerializedName("has_attachment")
    private HasAttachment hasAttachment;
    @Expose
    @SerializedName("is_imported")
    private IsImported isImported;
    @Expose
    @SerializedName("inbound_email_record")
    private InboundEmailRecord inboundEmailRecord;
    @Expose
    @SerializedName("folder_type")
    private FolderType folderType;
    @Expose
    @SerializedName("folder")
    private Folder folder;
    @Expose
    @SerializedName("msgno")
    private Msgno msgno;
    @Expose
    @SerializedName("uid")
    private Uid uid;
    @Expose
    @SerializedName("attachment")
    private Attachment attachment;
    @Expose
    @SerializedName("subject")
    private Subject subject;
    @Expose
    @SerializedName("indicator")
    private Indicator indicator;
    @Expose
    @SerializedName("parent_id")
    private ParentId parentId;
    @Expose
    @SerializedName("parent_type")
    private ParentType parentType;
    @Expose
    @SerializedName("parent_name")
    private ParentName parentName;
    @Expose
    @SerializedName("mailbox_id")
    private MailboxId mailboxId;
    @Expose
    @SerializedName("intent")
    private Intent intent;
    @Expose
    @SerializedName("reply_to_status")
    private ReplyToStatus replyToStatus;
    @Expose
    @SerializedName("flagged")
    private Flagged flagged;
    @Expose
    @SerializedName("status")
    private String status;
    @Expose
    @SerializedName("type")
    private Type type;
    @Expose
    @SerializedName("message_id")
    private MessageId messageId;
    @Expose
    @SerializedName("date_sent_received")
    private DateSentReceived dateSentReceived;
    @Expose
    @SerializedName("description_html")
    private DescriptionHtml descriptionHtml;
    @Expose
    @SerializedName("raw_source")
    private RawSource rawSource;
    @Expose
    @SerializedName("imap_keywords")
    private ImapKeywords imapKeywords;
    @Expose
    @SerializedName("bcc_addrs_names")
    private BccAddrsNames bccAddrsNames;
    @Expose
    @SerializedName("cc_addrs_names")
    private CcAddrsNames ccAddrsNames;
    @Expose
    @SerializedName("to_addrs_names")
    private ToAddrsNames toAddrsNames;
    @Expose
    @SerializedName("reply_to_addr")
    private ReplyToAddr replyToAddr;
    @Expose
    @SerializedName("from_addr_name")
    private FromAddrName fromAddrName;
    @Expose
    @SerializedName("last_synced")
    private LastSynced lastSynced;
    @Expose
    @SerializedName("orphaned")
    private Orphaned orphaned;
    @Expose
    @SerializedName("assigned_user_id")
    private AssignedUserId assignedUserId;
    @Expose
    @SerializedName("deleted")
    private Deleted deleted;
    @Expose
    @SerializedName("description")
    private Description description;
    @Expose
    @SerializedName("created_by")
    private CreatedBy createdBy;
    @Expose
    @SerializedName("modified_user_id")
    private ModifiedUserId modifiedUserId;
    @Expose
    @SerializedName("date_modified")
    private DateModified dateModified;
    @Expose
    @SerializedName("date_entered")
    private DateEntered dateEntered;
    @Expose
    @SerializedName("name")
    private Name name;
    @Expose
    @SerializedName("id")
    private Id id;
    @Expose
    @SerializedName("created_by_name")
    private CreatedByName createdByName;
    @Expose
    @SerializedName("modified_by_name")
    private ModifiedByName modifiedByName;
    @Expose
    @SerializedName("assigned_user_name")
    private AssignedUserName assignedUserName;

    public OptIn getOptIn() {
        return optIn;
    }

    public void setOptIn(OptIn optIn) {
        this.optIn = optIn;
    }

    public EmailsEmailTemplatesName getEmailsEmailTemplatesName() {
        return emailsEmailTemplatesName;
    }

    public void setEmailsEmailTemplatesName(EmailsEmailTemplatesName emailsEmailTemplatesName) {
        this.emailsEmailTemplatesName = emailsEmailTemplatesName;
    }

    public CategoryId getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(CategoryId categoryId) {
        this.categoryId = categoryId;
    }

    public IsOnlyPlainText getIsOnlyPlainText() {
        return isOnlyPlainText;
    }

    public void setIsOnlyPlainText(IsOnlyPlainText isOnlyPlainText) {
        this.isOnlyPlainText = isOnlyPlainText;
    }

    public HasAttachment getHasAttachment() {
        return hasAttachment;
    }

    public void setHasAttachment(HasAttachment hasAttachment) {
        this.hasAttachment = hasAttachment;
    }

    public IsImported getIsImported() {
        return isImported;
    }

    public void setIsImported(IsImported isImported) {
        this.isImported = isImported;
    }

    public InboundEmailRecord getInboundEmailRecord() {
        return inboundEmailRecord;
    }

    public void setInboundEmailRecord(InboundEmailRecord inboundEmailRecord) {
        this.inboundEmailRecord = inboundEmailRecord;
    }

    public FolderType getFolderType() {
        return folderType;
    }

    public void setFolderType(FolderType folderType) {
        this.folderType = folderType;
    }

    public Folder getFolder() {
        return folder;
    }

    public void setFolder(Folder folder) {
        this.folder = folder;
    }

    public Msgno getMsgno() {
        return msgno;
    }

    public void setMsgno(Msgno msgno) {
        this.msgno = msgno;
    }

    public Uid getUid() {
        return uid;
    }

    public void setUid(Uid uid) {
        this.uid = uid;
    }

    public Attachment getAttachment() {
        return attachment;
    }

    public void setAttachment(Attachment attachment) {
        this.attachment = attachment;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public Indicator getIndicator() {
        return indicator;
    }

    public void setIndicator(Indicator indicator) {
        this.indicator = indicator;
    }


//    public ParentType getParentType() {
//        return parentType;
//    }

    public void setParentType(ParentType parentType) {
        this.parentType = parentType;
    }

//    public ParentName getParentName() {
//        return parentName;
//    }

    public void setParentName(ParentName parentName) {
        this.parentName = parentName;
    }

    public MailboxId getMailboxId() {
        return mailboxId;
    }

    public void setMailboxId(MailboxId mailboxId) {
        this.mailboxId = mailboxId;
    }

    public Intent getIntent() {
        return intent;
    }

    public void setIntent(Intent intent) {
        this.intent = intent;
    }

    public ReplyToStatus getReplyToStatus() {
        return replyToStatus;
    }

    public void setReplyToStatus(ReplyToStatus replyToStatus) {
        this.replyToStatus = replyToStatus;
    }

    public Flagged getFlagged() {
        return flagged;
    }

    public void setFlagged(Flagged flagged) {
        this.flagged = flagged;
    }

//    public Status getStatus() {
//        return status;
//    }

//    public void setStatus(Status status) {
//        this.status = status;
//    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public MessageId getMessageId() {
        return messageId;
    }

    public void setMessageId(MessageId messageId) {
        this.messageId = messageId;
    }

    public DateSentReceived getDateSentReceived() {
        return dateSentReceived;
    }

    public void setDateSentReceived(DateSentReceived dateSentReceived) {
        this.dateSentReceived = dateSentReceived;
    }

    public DescriptionHtml getDescriptionHtml() {
        return descriptionHtml;
    }

    public void setDescriptionHtml(DescriptionHtml descriptionHtml) {
        this.descriptionHtml = descriptionHtml;
    }

    public RawSource getRawSource() {
        return rawSource;
    }

    public void setRawSource(RawSource rawSource) {
        this.rawSource = rawSource;
    }

    public ImapKeywords getImapKeywords() {
        return imapKeywords;
    }

    public void setImapKeywords(ImapKeywords imapKeywords) {
        this.imapKeywords = imapKeywords;
    }

    public BccAddrsNames getBccAddrsNames() {
        return bccAddrsNames;
    }

    public void setBccAddrsNames(BccAddrsNames bccAddrsNames) {
        this.bccAddrsNames = bccAddrsNames;
    }

    public CcAddrsNames getCcAddrsNames() {
        return ccAddrsNames;
    }

    public void setCcAddrsNames(CcAddrsNames ccAddrsNames) {
        this.ccAddrsNames = ccAddrsNames;
    }

    public ToAddrsNames getToAddrsNames() {
        return toAddrsNames;
    }

    public void setToAddrsNames(ToAddrsNames toAddrsNames) {
        this.toAddrsNames = toAddrsNames;
    }

    public ReplyToAddr getReplyToAddr() {
        return replyToAddr;
    }

    public void setReplyToAddr(ReplyToAddr replyToAddr) {
        this.replyToAddr = replyToAddr;
    }

    public FromAddrName getFromAddrName() {
        return fromAddrName;
    }

    public void setFromAddrName(FromAddrName fromAddrName) {
        this.fromAddrName = fromAddrName;
    }

    public LastSynced getLastSynced() {
        return lastSynced;
    }

    public void setLastSynced(LastSynced lastSynced) {
        this.lastSynced = lastSynced;
    }

    public Orphaned getOrphaned() {
        return orphaned;
    }

    public void setOrphaned(Orphaned orphaned) {
        this.orphaned = orphaned;
    }

    public AssignedUserId getAssignedUserId() {
        return assignedUserId;
    }

    public void setAssignedUserId(AssignedUserId assignedUserId) {
        this.assignedUserId = assignedUserId;
    }

    public Deleted getDeleted() {
        return deleted;
    }

    public void setDeleted(Deleted deleted) {
        this.deleted = deleted;
    }

    public Description getDescription() {
        return description;
    }

    public void setDescription(Description description) {
        this.description = description;
    }

    public CreatedBy getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(CreatedBy createdBy) {
        this.createdBy = createdBy;
    }

    public ModifiedUserId getModifiedUserId() {
        return modifiedUserId;
    }

    public void setModifiedUserId(ModifiedUserId modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    public DateModified getDateModified() {
        return dateModified;
    }

    public void setDateModified(DateModified dateModified) {
        this.dateModified = dateModified;
    }

    public DateEntered getDateEntered() {
        return dateEntered;
    }

    public void setDateEntered(DateEntered dateEntered) {
        this.dateEntered = dateEntered;
    }

    public Name getName() {
        return name;
    }

    public void setName(Name name) {
        this.name = name;
    }

    public Id getId() {
        return id;
    }

    public void setId(Id id) {
        this.id = id;
    }

    public CreatedByName getCreatedByName() {
        return createdByName;
    }

    public void setCreatedByName(CreatedByName createdByName) {
        this.createdByName = createdByName;
    }

    public ModifiedByName getModifiedByName() {
        return modifiedByName;
    }

    public void setModifiedByName(ModifiedByName modifiedByName) {
        this.modifiedByName = modifiedByName;
    }

    public AssignedUserName getAssignedUserName() {
        return assignedUserName;
    }

    public void setAssignedUserName(AssignedUserName assignedUserName) {
        this.assignedUserName = assignedUserName;
    }
//
//    @SerializedName("assigned_user_name")
//    @Expose
//    private String assignedUserName;
//    @SerializedName("modified_by_name")
//    @Expose
//    private String modifiedByName;
//    @SerializedName("created_by_name")
//    @Expose
//    private String createdByName;
//    @SerializedName("id")
//    @Expose
//    private String id;
//    @SerializedName("name")
//    @Expose
//    private String name;
//    @SerializedName("date_entered")
//    @Expose
//    private String dateEntered;
//    @SerializedName("date_modified")
//    @Expose
//    private String dateModified;
//    @SerializedName("modified_user_id")
//    @Expose
//    private String modifiedUserId;
//    @SerializedName("created_by")
//    @Expose
//    private String createdBy;
//    @SerializedName("description")
//    @Expose
//    private String description;
//    @SerializedName("deleted")
//    @Expose
//    private String deleted;
//    @SerializedName("assigned_user_id")
//    @Expose
//    private String assignedUserId;
    @SerializedName("duration_hours")
    @Expose
    private String durationHours;
    @SerializedName("duration_minutes")
    @Expose
    private String durationMinutes;
    @SerializedName("date_start")
    @Expose
    private String dateStart;
    @SerializedName("date_end")
    @Expose
    private String dateEnd;
//    @SerializedName("parent_type")
//    @Expose
//    private String parentType;
//    @SerializedName("parent_name")
//    @Expose
//    private String parentName;
//    @SerializedName("status")
//    @Expose
//    private String status;
    @SerializedName("direction")
    @Expose
    private String direction;
//    @SerializedName("parent_id")
//    @Expose
//    private String parentId;
    @SerializedName("reminder_checked")
    @Expose
    private Boolean reminderChecked;
    @SerializedName("reminder_time")
    @Expose
    private String reminderTime;
    @SerializedName("email_reminder_checked")
    @Expose
    private Boolean emailReminderChecked;
    @SerializedName("email_reminder_time")
    @Expose
    private String emailReminderTime;
    @SerializedName("email_reminder_sent")
    @Expose
    private String emailReminderSent;
    @SerializedName("reminders")
    @Expose
    private String reminders;
    @SerializedName("outlook_id")
    @Expose
    private String outlookId;
    @SerializedName("accept_status")
    @Expose
    private String acceptStatus;
    @SerializedName("contact_name")
    @Expose
    private String contactName;
    @SerializedName("contact_id")
    @Expose
    private String contactId;
    @SerializedName("repeat_type")
    @Expose
    private String repeatType;
    @SerializedName("repeat_interval")
    @Expose
    private String repeatInterval;
    @SerializedName("repeat_dow")
    @Expose
    private String repeatDow;
    @SerializedName("repeat_until")
    @Expose
    private String repeatUntil;
    @SerializedName("repeat_count")
    @Expose
    private String repeatCount;
    @SerializedName("repeat_parent_id")
    @Expose
    private String repeatParentId;
    @SerializedName("recurring_source")
    @Expose
    private String recurringSource;
    @SerializedName("reschedule_history")
    @Expose
    private String rescheduleHistory;
    @SerializedName("reschedule_count")
    @Expose
    private String rescheduleCount;
//
//    public String getAssignedUserName() {
//        return assignedUserName;
//    }
//
//    public void setAssignedUserName(String assignedUserName) {
//        this.assignedUserName = assignedUserName;
//    }

//    public String getModifiedByName() {
//        return modifiedByName;
//    }
//
//    public void setModifiedByName(String modifiedByName) {
//        this.modifiedByName = modifiedByName;
//    }
//
//    public String getCreatedByName()     {
//        return createdByName;
//    }
//
//    public void setCreatedByName(String createdByName) {
//        this.createdByName = createdByName;
//    }
//
//    public String getId() {
//        return id;
//    }
//
//    public void setId(String id) {
//        this.id = id;
//    }

//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public String getDateEntered() {
//        return dateEntered;
//    }
//
//    public void setDateEntered(String dateEntered) {
//        this.dateEntered = dateEntered;
//    }

//    public String getDateModified() {
//        return dateModified;
//    }
//
//    public void setDateModified(String dateModified) {
//        this.dateModified = dateModified;
//    }
//
//    public String getModifiedUserId() {
//        return modifiedUserId;
//    }
//
//    public void setModifiedUserId(String modifiedUserId) {
//        this.modifiedUserId = modifiedUserId;
//    }

//    public String getCreatedBy() {
//        return createdBy;
//    }
//
//    public void setCreatedBy(String createdBy) {
//        this.createdBy = createdBy;
//    }
//
//    public String getDescription() {
//        return description;
//    }
//
//    public void setDescription(String description) {
//        this.description = description;
//    }

//    public String getDeleted() {
//        return deleted;
//    }
//
//    public void setDeleted(String deleted) {
//        this.deleted = deleted;
//    }
//
//    public String getAssignedUserId() {
//        return assignedUserId;
//    }
//
//    public void setAssignedUserId(String assignedUserId) {
//        this.assignedUserId = assignedUserId;
//    }

    public String getDurationHours() {
        return durationHours;
    }

    public void setDurationHours(String durationHours) {
        this.durationHours = durationHours;
    }

    public String getDurationMinutes() {
        return durationMinutes;
    }

    public void setDurationMinutes(String durationMinutes) {
        this.durationMinutes = durationMinutes;
    }

    public String getDateStart() {
        return dateStart;
    }

    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    public String getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

//    public String getParentType() {
//        return parentType;
//    }
//
//    public void setParentType(String parentType) {
//        this.parentType = parentType;
//    }
//
//    public String getParentName() {
//        return parentName;
//    }

//    public void setParentName(String parentName) {
//        this.parentName = parentName;
//    }
//
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

//    public String getDirection(){
//        return direction;
//    }
//
//    public void setDirection(String direction) {
//        this.direction = direction;
//    }

//    public String getParentId() {
//        return parentId;
//    }
//
//    public void setParentId(String parentId) {
//        this.parentId = parentId;
//    }

    public Boolean getReminderChecked() {
        return reminderChecked;
    }

    public void setReminderChecked(Boolean reminderChecked) {
        this.reminderChecked = reminderChecked;
    }

    public String getReminderTime() {
        return reminderTime;
    }

    public void setReminderTime(String reminderTime) {
        this.reminderTime = reminderTime;
    }

    public Boolean getEmailReminderChecked() {
        return emailReminderChecked;
    }

    public void setEmailReminderChecked(Boolean emailReminderChecked) {
        this.emailReminderChecked = emailReminderChecked;
    }

    public String getEmailReminderTime() {
        return emailReminderTime;
    }

    public void setEmailReminderTime(String emailReminderTime) {
        this.emailReminderTime = emailReminderTime;
    }

    public String getEmailReminderSent() {
        return emailReminderSent;
    }

    public void setEmailReminderSent(String emailReminderSent) {
        this.emailReminderSent = emailReminderSent;
    }

    public String getReminders() {
        return reminders;
    }

    public void setReminders(String reminders) {
        this.reminders = reminders;
    }

    public String getOutlookId() {
        return outlookId;
    }

    public void setOutlookId(String outlookId) {
        this.outlookId = outlookId;
    }

    public String getAcceptStatus() {
        return acceptStatus;
    }

    public void setAcceptStatus(String acceptStatus) {
        this.acceptStatus = acceptStatus;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    public String getRepeatType() {
        return repeatType;
    }

    public void setRepeatType(String repeatType) {
        this.repeatType = repeatType;
    }

    public String getRepeatInterval() {
        return repeatInterval;
    }

    public void setRepeatInterval(String repeatInterval) {
        this.repeatInterval = repeatInterval;
    }

    public String getRepeatDow() {
        return repeatDow;
    }

    public void setRepeatDow(String repeatDow) {
        this.repeatDow = repeatDow;
    }

    public String getRepeatUntil() {
        return repeatUntil;
    }

    public void setRepeatUntil(String repeatUntil) {
        this.repeatUntil = repeatUntil;
    }

    public String getRepeatCount() {
        return repeatCount;
    }

    public void setRepeatCount(String repeatCount) {
        this.repeatCount = repeatCount;
    }

    public String getRepeatParentId() {
        return repeatParentId;
    }

    public void setRepeatParentId(String repeatParentId) {
        this.repeatParentId = repeatParentId;
    }

    public String getRecurringSource() {
        return recurringSource;
    }

    public void setRecurringSource(String recurringSource) {
        this.recurringSource = recurringSource;
    }

    public String getRescheduleHistory() {
        return rescheduleHistory;
    }

    public void setRescheduleHistory(String rescheduleHistory) {
        this.rescheduleHistory = rescheduleHistory;
    }

    public String getRescheduleCount() {
        return rescheduleCount;
    }

    public void setRescheduleCount(String rescheduleCount) {
        this.rescheduleCount = rescheduleCount;
    }

}
