package com.rohit.contactsapp.models;

public class Modelsforfeeds {
    private int image;
    private String name;
    private String post_date;
    private String feed;

    public Modelsforfeeds(int image, String name, String post_date, String feed) {
        this.image = image;
        this.name = name;
        this.post_date = post_date;
        this.feed = feed;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPost_date() {
        return post_date;
    }

    public void setPost_date(String post_date) {
        this.post_date = post_date;
    }

    public String getFeed() {
        return feed;
    }

    public void setFeed(String feed) {
        this.feed = feed;
    }
}
