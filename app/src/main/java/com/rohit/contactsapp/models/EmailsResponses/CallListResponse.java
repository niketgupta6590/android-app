
package com.rohit.contactsapp.models.EmailsResponses;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CallListResponse {

    @SerializedName("result_count")
    @Expose
    private Integer resultCount;
    @SerializedName("total_count")
    @Expose
    private String totalCount;
    @SerializedName("next_offset")
    @Expose
    private Integer nextOffset;
    @SerializedName("entry_list")
    @Expose
    private List<EntryList> entryList = null;

    public Integer getResultCount() {
        return resultCount;
    }

    public void setResultCount(Integer resultCount) {
        this.resultCount = resultCount;
    }

    public String getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }

    public Integer getNextOffset() {
        return nextOffset;
    }

    public void setNextOffset(Integer nextOffset) {
        this.nextOffset = nextOffset;
    }

    public List<EntryList> getEntryList() {
        return entryList;
    }

    public void setEntryList(List<EntryList> entryList) {
        this.entryList = entryList;
    }

}
