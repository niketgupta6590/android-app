package com.rohit.contactsapp.models.EmailsResponses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Login {


    public static class LoginRequest{


        private String input_type;
        private String response_type;
        private String method;
        private String user_name;
        private String password;
        private String encryption;
        private String application;

        public String getInput_type() {
            return input_type;
        }

        public void setInput_type(String input_type) {
            this.input_type = input_type;
        }

        public String getResponse_type() {
            return response_type;
        }

        public void setResponse_type(String response_type) {
            this.response_type = response_type;
        }

        public String getMethod() {
            return method;
        }

        public void setMethod(String method) {
            this.method = method;
        }

        public String getUser_name() {
            return user_name;
        }

        public void setUser_name(String user_name) {
            this.user_name = user_name;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getEncryption() {
            return encryption;
        }

        public void setEncryption(String encryption) {
            this.encryption = encryption;
        }

        public String getApplication() {
            return application;
        }

        public void setApplication(String application) {
            this.application = application;
        }
    }

    //////////////////////////////////////
    public static  class LoginResponse {


        @Expose
        @SerializedName("name_value_list")
        private NameValueList nameValueList;
        @Expose
        @SerializedName("module_name")
        private String moduleName;
        @Expose
        @SerializedName("id")
        private String id;

        public NameValueList getNameValueList() {
            return nameValueList;
        }

        public void setNameValueList(NameValueList nameValueList) {
            this.nameValueList = nameValueList;
        }

        public String getModuleName() {
            return moduleName;
        }

        public void setModuleName(String moduleName) {
            this.moduleName = moduleName;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public static class NameValueList {
            @Expose
            @SerializedName("user_currency_name")
            private UserCurrencyName userCurrencyName;
            @Expose
            @SerializedName("mobile_max_subpanel_entries")
            private MobileMaxSubpanelEntries mobileMaxSubpanelEntries;
            @Expose
            @SerializedName("mobile_max_list_entries")
            private MobileMaxListEntries mobileMaxListEntries;
            @Expose
            @SerializedName("user_decimal_separator")
            private UserDecimalSeparator userDecimalSeparator;
            @Expose
            @SerializedName("user_number_separator")
            private UserNumberSeparator userNumberSeparator;
            @Expose
            @SerializedName("user_default_timeformat")
            private UserDefaultTimeformat userDefaultTimeformat;
            @Expose
            @SerializedName("user_default_dateformat")
            private UserDefaultDateformat userDefaultDateformat;
            @Expose
            @SerializedName("user_default_team_id")
            private UserDefaultTeamId userDefaultTeamId;
            @Expose
            @SerializedName("user_is_admin")
            private UserIsAdmin userIsAdmin;
            @Expose
            @SerializedName("user_currency_id")
            private UserCurrencyId userCurrencyId;
            @Expose
            @SerializedName("user_language")
            private UserLanguage userLanguage;
            @Expose
            @SerializedName("user_name")
            private UserName userName;
            @Expose
            @SerializedName("user_id")
            private UserId userId;

            public UserCurrencyName getUserCurrencyName() {
                return userCurrencyName;
            }

            public void setUserCurrencyName(UserCurrencyName userCurrencyName) {
                this.userCurrencyName = userCurrencyName;
            }

            public MobileMaxSubpanelEntries getMobileMaxSubpanelEntries() {
                return mobileMaxSubpanelEntries;
            }

            public void setMobileMaxSubpanelEntries(MobileMaxSubpanelEntries mobileMaxSubpanelEntries) {
                this.mobileMaxSubpanelEntries = mobileMaxSubpanelEntries;
            }

            public MobileMaxListEntries getMobileMaxListEntries() {
                return mobileMaxListEntries;
            }

            public void setMobileMaxListEntries(MobileMaxListEntries mobileMaxListEntries) {
                this.mobileMaxListEntries = mobileMaxListEntries;
            }

            public UserDecimalSeparator getUserDecimalSeparator() {
                return userDecimalSeparator;
            }

            public void setUserDecimalSeparator(UserDecimalSeparator userDecimalSeparator) {
                this.userDecimalSeparator = userDecimalSeparator;
            }

            public UserNumberSeparator getUserNumberSeparator() {
                return userNumberSeparator;
            }

            public void setUserNumberSeparator(UserNumberSeparator userNumberSeparator) {
                this.userNumberSeparator = userNumberSeparator;
            }

            public UserDefaultTimeformat getUserDefaultTimeformat() {
                return userDefaultTimeformat;
            }

            public void setUserDefaultTimeformat(UserDefaultTimeformat userDefaultTimeformat) {
                this.userDefaultTimeformat = userDefaultTimeformat;
            }

            public UserDefaultDateformat getUserDefaultDateformat() {
                return userDefaultDateformat;
            }

            public void setUserDefaultDateformat(UserDefaultDateformat userDefaultDateformat) {
                this.userDefaultDateformat = userDefaultDateformat;
            }

            public UserDefaultTeamId getUserDefaultTeamId() {
                return userDefaultTeamId;
            }

            public void setUserDefaultTeamId(UserDefaultTeamId userDefaultTeamId) {
                this.userDefaultTeamId = userDefaultTeamId;
            }

            public UserIsAdmin getUserIsAdmin() {
                return userIsAdmin;
            }

            public void setUserIsAdmin(UserIsAdmin userIsAdmin) {
                this.userIsAdmin = userIsAdmin;
            }

            public UserCurrencyId getUserCurrencyId() {
                return userCurrencyId;
            }

            public void setUserCurrencyId(UserCurrencyId userCurrencyId) {
                this.userCurrencyId = userCurrencyId;
            }

            public UserLanguage getUserLanguage() {
                return userLanguage;
            }

            public void setUserLanguage(UserLanguage userLanguage) {
                this.userLanguage = userLanguage;
            }

            public UserName getUserName() {
                return userName;
            }

            public void setUserName(UserName userName) {
                this.userName = userName;
            }

            public UserId getUserId() {
                return userId;
            }

            public void setUserId(UserId userId) {
                this.userId = userId;
            }
        }

        public static class UserCurrencyName {
            @Expose
            @SerializedName("value")
            private String value;
            @Expose
            @SerializedName("name")
            private String name;

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }

        public static class MobileMaxSubpanelEntries {
            @Expose
            @SerializedName("name")
            private String name;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }

        public static class MobileMaxListEntries {
            @Expose
            @SerializedName("name")
            private String name;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }

        public static class UserDecimalSeparator {
            @Expose
            @SerializedName("value")
            private String value;
            @Expose
            @SerializedName("name")
            private String name;

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }

        public static class UserNumberSeparator {
            @Expose
            @SerializedName("value")
            private String value;
            @Expose
            @SerializedName("name")
            private String name;

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }

        public static class UserDefaultTimeformat {
            @Expose
            @SerializedName("value")
            private String value;
            @Expose
            @SerializedName("name")
            private String name;

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }

        public static class UserDefaultDateformat {
            @Expose
            @SerializedName("value")
            private String value;
            @Expose
            @SerializedName("name")
            private String name;

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }

        public static class UserDefaultTeamId {
            @Expose
            @SerializedName("name")
            private String name;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }

        public static class UserIsAdmin {
            @Expose
            @SerializedName("value")
            private boolean value;
            @Expose
            @SerializedName("name")
            private String name;

            public boolean getValue() {
                return value;
            }

            public void setValue(boolean value) {
                this.value = value;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }

        public static class UserCurrencyId {
            @Expose
            @SerializedName("value")
            private String value;
            @Expose
            @SerializedName("name")
            private String name;

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }

        public static class UserLanguage {
            @Expose
            @SerializedName("value")
            private String value;
            @Expose
            @SerializedName("name")
            private String name;

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }

        public static class UserName {
            @Expose
            @SerializedName("value")
            private String value;
            @Expose
            @SerializedName("name")
            private String name;

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }

        public static class UserId {
            @Expose
            @SerializedName("value")
            private String value;
            @Expose
            @SerializedName("name")
            private String name;

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }
    }
}
