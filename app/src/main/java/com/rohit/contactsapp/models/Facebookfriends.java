package com.rohit.contactsapp.models;

public class Facebookfriends {

    private String id;
    private String name;

    public Facebookfriends(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
