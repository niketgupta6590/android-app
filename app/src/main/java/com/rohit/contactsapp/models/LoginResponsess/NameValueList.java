
package com.rohit.contactsapp.models.LoginResponsess;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NameValueList {

    @SerializedName("user_id")
    @Expose
    private UserId userId;
    @SerializedName("user_name")
    @Expose
    private UserName userName;
    @SerializedName("user_language")
    @Expose
    private UserLanguage userLanguage;
    @SerializedName("user_currency_id")
    @Expose
    private UserCurrencyId userCurrencyId;
    @SerializedName("user_is_admin")
    @Expose
    private UserIsAdmin userIsAdmin;
    @SerializedName("user_default_team_id")
    @Expose
    private UserDefaultTeamId userDefaultTeamId;
    @SerializedName("user_default_dateformat")
    @Expose
    private UserDefaultDateformat userDefaultDateformat;
    @SerializedName("user_default_timeformat")
    @Expose
    private UserDefaultTimeformat userDefaultTimeformat;
    @SerializedName("user_number_separator")
    @Expose
    private UserNumberSeparator userNumberSeparator;
    @SerializedName("user_decimal_separator")
    @Expose
    private UserDecimalSeparator userDecimalSeparator;
    @SerializedName("mobile_max_list_entries")
    @Expose
    private MobileMaxListEntries mobileMaxListEntries;
    @SerializedName("mobile_max_subpanel_entries")
    @Expose
    private MobileMaxSubpanelEntries mobileMaxSubpanelEntries;
    @SerializedName("user_currency_name")
    @Expose
    private UserCurrencyName userCurrencyName;

    public UserId getUserId() {
        return userId;
    }

    public void setUserId(UserId userId) {
        this.userId = userId;
    }

    public UserName getUserName() {
        return userName;
    }

    public void setUserName(UserName userName) {
        this.userName = userName;
    }

    public UserLanguage getUserLanguage() {
        return userLanguage;
    }

    public void setUserLanguage(UserLanguage userLanguage) {
        this.userLanguage = userLanguage;
    }

    public UserCurrencyId getUserCurrencyId() {
        return userCurrencyId;
    }

    public void setUserCurrencyId(UserCurrencyId userCurrencyId) {
        this.userCurrencyId = userCurrencyId;
    }

    public UserIsAdmin getUserIsAdmin() {
        return userIsAdmin;
    }

    public void setUserIsAdmin(UserIsAdmin userIsAdmin) {
        this.userIsAdmin = userIsAdmin;
    }

    public UserDefaultTeamId getUserDefaultTeamId() {
        return userDefaultTeamId;
    }

    public void setUserDefaultTeamId(UserDefaultTeamId userDefaultTeamId) {
        this.userDefaultTeamId = userDefaultTeamId;
    }

    public UserDefaultDateformat getUserDefaultDateformat() {
        return userDefaultDateformat;
    }

    public void setUserDefaultDateformat(UserDefaultDateformat userDefaultDateformat) {
        this.userDefaultDateformat = userDefaultDateformat;
    }

    public UserDefaultTimeformat getUserDefaultTimeformat() {
        return userDefaultTimeformat;
    }

    public void setUserDefaultTimeformat(UserDefaultTimeformat userDefaultTimeformat) {
        this.userDefaultTimeformat = userDefaultTimeformat;
    }

    public UserNumberSeparator getUserNumberSeparator() {
        return userNumberSeparator;
    }

    public void setUserNumberSeparator(UserNumberSeparator userNumberSeparator) {
        this.userNumberSeparator = userNumberSeparator;
    }

    public UserDecimalSeparator getUserDecimalSeparator() {
        return userDecimalSeparator;
    }

    public void setUserDecimalSeparator(UserDecimalSeparator userDecimalSeparator) {
        this.userDecimalSeparator = userDecimalSeparator;
    }

    public MobileMaxListEntries getMobileMaxListEntries() {
        return mobileMaxListEntries;
    }

    public void setMobileMaxListEntries(MobileMaxListEntries mobileMaxListEntries) {
        this.mobileMaxListEntries = mobileMaxListEntries;
    }

    public MobileMaxSubpanelEntries getMobileMaxSubpanelEntries() {
        return mobileMaxSubpanelEntries;
    }

    public void setMobileMaxSubpanelEntries(MobileMaxSubpanelEntries mobileMaxSubpanelEntries) {
        this.mobileMaxSubpanelEntries = mobileMaxSubpanelEntries;
    }

    public UserCurrencyName getUserCurrencyName() {
        return userCurrencyName;
    }

    public void setUserCurrencyName(UserCurrencyName userCurrencyName) {
        this.userCurrencyName = userCurrencyName;
    }

}
