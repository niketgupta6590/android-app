package com.rohit.contactsapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;

public class Models {

    @SerializedName("modules")
    @Expose
    private ArrayList<Modules> modules = null;

    public Models(ArrayList<Modules> modules) {
        this.modules = modules;
    }

    public ArrayList<Modules> getModules() {
        return modules;
    }

    public void setModules(ArrayList<Modules> modules) {
        this.modules = modules;
    }

}
