
package com.rohit.contactsapp.models.EmailsResponses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EntryList {
    @Expose
    @SerializedName("name_value_list")
    private NameValueList nameValueList;
    @Expose
    @SerializedName("module_name")
    private String moduleName;
    @Expose
    @SerializedName("id")
    private String id;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public NameValueList getNameValueList() {
        return nameValueList;
    }

    public void setNameValueList(NameValueList nameValueList) {
        this.nameValueList = nameValueList;
    }

}
